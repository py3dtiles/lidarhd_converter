import os
import re

from setuptools import find_packages, setup

here = os.path.abspath(os.path.dirname(__file__))

requirements = (
'GDAL==3.2.2',
'py7zr',
'requests',
'tqdm',
'py3dtiles==5.0.0',
'laspy[laszip]',
'python-dotenv',
)



def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


def find_version(*file_paths):
    """
    see https://github.com/pypa/sampleproject/blob/master/setup.py
    """

    with open(os.path.join(here, *file_paths), 'r') as f:
        version_file = f.read()

    # The version line must have the form
    # __version__ = 'ver'
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string. "
                       "Should be at the first line of __init__.py.")


setup(
    name='lidarhd_converter',
    version='0.1.0',
    description="Python cli and api to extract lidar hd and convert them to 3Dtiles",
    url='https://gitlab.com/py3dtiles/lidarhd_converter',
    author='Oslandia',
    author_email='contact@oslandia.com',
    python_requires='>=3.7',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3.7',
    ],
    packages=find_packages(),
    install_requires=requirements,
    entry_points={
        'console_scripts': ['lidarhd_converter=lidarhd_converter.command_line:main'],
    },
)
