import io
import os
import re
import tempfile
import time
import zipfile
from pathlib import Path
from time import sleep

from osgeo import ogr
from pyproj import CRS
import py7zr
import requests
import tqdm


from py3dtiles import convert, merger


def init_parser(parser):
    parser.add_argument('geometry_file', type=str, help="Le fichier contenant les géométries permettant de filtrer les parties du lidarhd. Seul les fichiers intersectant cette géométrie seront téléchargés et convertis. Le CRS de la géométrie doit être le même que celui du lidar hd lui-même (EPSG:2154)")
    parser.add_argument('out_folder', type=Path, help="Le dossier où les tilesets seront synchronisés")
    parser.add_argument('-t', '--tmp-directory', type=Path, help="Le dossier où les fichiers temporaires seront écrits. Peut être renseigné avec la variable d'environnement TMP_DIRECTORY")
    parser.add_argument('-m', '--lidar-metadata-url', type=str, help="L'url complète pour télécharger les métadonnées du LidarHD. Peut être renseigné avec la variable d'environnement LIDAR_METADATA_URL")
    parser.add_argument('-d', '--lidar-hd-endpoint', type=str, help="Le endpoint pour télécharger les fichiers LidarHD. Peut être renseigné avec la variable d'environnement LIDAR_HD_ENDPOINT. Il faut indiquer le protocole (http:// ou https://). Par défaut, l'url présent dans le catalogue n'est pas modifié.")
    parser.add_argument('--lidar-endpoint-regex', type=str, help="L'expression régulière utilisée pour remplacer les urls du catalogue. Utile si le catalogue utilisé n'est pas le catalogue public. Par défaut: https://wxs.ign.fr/.*/telechargement/prepackage/")
    parser.add_argument('-s', '--srs-out', type=str, help="Le SRS de destination. Peut être renseigné avec la variable d'environnement SRS_OUT")
    parser.add_argument('-f', '--force', action='store_true', help="Télécharge et convertit les fichiers LidarHD même s'ils existent déjà dans le dossier")
    parser.add_argument('-l', '--layer', type=int, help="L'index de la couche s'il y en a plusieurs dans le fichier 'geometry_file'")


# Sometimes the server returns a 502 Bad Gateway or 404 Not Found error
def get_url_or_retry(url, nb_attempt=3, no_verify_ssl=False):
    for _ in range(nb_attempt):
        response = requests.get(url, allow_redirects=True, verify=(not no_verify_ssl))
        if response.ok:
            break
        else:
            print(f"Réponse {response.status_code}, nouvelle tentative dans 10sec")
            sleep(10)
    else:
        print(
            f"Échec du téléchargement du fichier {url}, le serveur a répondu:\n"
            f"{response.status_code} {response.reason}"
        )
    return response


def print_stats(download_time, uncompress_time, conversion_time):
    print('Statistiques :')
    print('==============')
    print(f"- téléchargement: {download_time} secondes")
    print(f"- décompression: {uncompress_time} secondes")
    print(f"- conversion: {conversion_time} secondes")

def import_lidar(args):
    # Set variable from CLI argument if set, else if from env var, else None
    tmp_directory = args.tmp_directory or os.getenv('TMP_DIRECTORY')
    url_lidar_hd_grid = args.lidar_metadata_url or os.getenv('LIDAR_METADATA_URL') or "https://pcrs.ign.fr/download/lidar/shp"
    lidar_hd_endpoint = args.lidar_hd_endpoint or os.getenv('LIDAR_HD_ENDPOINT') or None
    url_pattern = re.compile(args.lidar_endpoint_regex or (os.getenv('LIDAR_ENDPOINT_REGEX')) or "https://wxs.ign.fr/.*/telechargement/prepackage/")
    no_verify_ssl = args.no_verify_ssl or (os.getenv('NO_VERIFY_SSL') is not None)
    if lidar_hd_endpoint and not lidar_hd_endpoint.endswith('/'):
        lidar_hd_endpoint += '/'
    srs_out = args.srs_out or os.getenv('SRS_OUT')

    if tmp_directory is not None:
        tmp_directory = Path(tmp_directory)
        if not tmp_directory.exists():
            raise FileNotFoundError(f"Le dossier {tmp_directory} n'existe pas")
        elif not tmp_directory.is_dir():
            raise NotADirectoryError(f"Le chemin {tmp_directory} n'est pas un dossier")

    # Try to create the ouptut folder, if it doesn't exist
    args.out_folder.mkdir(parents=True, exist_ok=True)

    # Read the input geometries
    print(f"Ouvre le fichier {args.geometry_file}")
    geometry_file = ogr.Open(args.geometry_file)
    if geometry_file is None:
        raise ValueError(f"Le fichier {args.geometry_file} ne peut pas être lu")

    # If there is one layer, take the first one else ask to the user the layer index
    if geometry_file.GetLayerCount() == 1:
        input_features = geometry_file.GetLayer(0)
    else:
        if args.layer is None:
            raise ValueError("L'argument --layer doit être spécifié lorsqu'il y a plusieurs couches dans geometry_file")
        input_features = geometry_file.GetLayer(args.layer)
        if input_features is None:
            raise ValueError(f"L'id {args.layer} ne correspond à aucune couche")


    # Try to download the lastest lidar hd metadata
    print("Télécharge les dernières métadonnées du lidar HD")

    # we retry 3 times before exit
    response = get_url_or_retry(url_lidar_hd_grid, no_verify_ssl=no_verify_ssl)
    if not response.ok:
        raise RuntimeError(f"Impossible de télécharger la ressource à partir de {url_lidar_hd_grid}")


    # Load the grid stored in the shapefile
    with tempfile.TemporaryDirectory(dir=tmp_directory) as directory: # at the end of the with, the directory and its content will be deleted
        directory_path = Path(directory)

        zip_stream = zipfile.ZipFile(io.BytesIO(response.content)) # avoid to write the file on the disk
        zip_stream.extractall(directory_path)

        shapefile_path = directory_path / 'TA_diff_pkk_lidarhd.shp'

        # we can't use io.BytesIO here since a shapefile is composed with many files
        lidar_hd_grid = ogr.Open(str(shapefile_path))

        # the data is loaded in lidar_hd_grid, the .shp can be deleted.

    # There is only one layer that contains the grid
    grid = lidar_hd_grid.GetLayer(0)


    print("Trouve toutes les cellules Lidar HD qui intersectent les géométries en entrée")

    lidar_urls = set() # to avoid duplicate url
    for feature in input_features:
        geometry = feature.GetGeometryRef()
        if not geometry: # in case where the geometry is NULL
            continue

        at_least_one_intersects = False
        for cell in grid:
            cell_geometry = cell.GetGeometryRef()
            if cell_geometry.Intersect(geometry):
                at_least_one_intersects = True
                output_tileset_json = args.out_folder / cell['nom_pkk'] / "tileset.json"
                print("Considère ", cell["nom_pkk"])
                if args.force or not output_tileset_json.exists():
                    lidar_urls.add((output_tileset_json.parent, cell['url_telech']))
                else:
                    print(f"-> {output_tileset_json.absolute()} existe, ignoré.")

    if len(lidar_urls) == 0 and not at_least_one_intersects:
        raise ValueError('Aucune cellule Lidar HD n\'intersecte ce polygone (mauvais CRS ?)')

    # Download, extract and move one by one lidar HD 7z archive in the specified directory.
    print("Télécharger les fichiers lidar HD", lidar_urls)
    download_time = 0
    uncompress_time = 0
    conversion_time = 0

    at_least_one_success = False
    for output_tileset_dir, url in tqdm.tqdm(lidar_urls): # tqdm will show the advancement
        with tempfile.TemporaryDirectory(dir=tmp_directory) as directory: # this directory will store all intermediate files (7z, laz)
            tmp_directory_path = Path(directory)

            if lidar_hd_endpoint:
                if not url_pattern.search(url):
                    raise ValueError("Le endpoint IGN du lidar HD a été changée. "
                    "url_pattern doit être mis à jour pour pouvoir correctement remplacer le endpoint par celui renseigné. "
                    f"Un exemple d'url IGN actuel : {url}")
                url = url_pattern.sub(lidar_hd_endpoint, url)

            start = time.time()
            response = get_url_or_retry(url, no_verify_ssl=no_verify_ssl)
            if not response.ok:
                print(f"Le fichier laz {url} n'a pas pu être téléchargé")
                continue
            end = time.time()

            download_time += end - start

            at_least_one_success = True
            print(f"Décompresse {url}")

            # avoid to write the file on the disk
            start = time.time()
            try:
                with py7zr.SevenZipFile(io.BytesIO(response.content)) as z:
                    z.extractall(tmp_directory_path)
            except py7zr.Bad7zFile as e:
                print(f"{url} corrompu, impossible de décompresser")
                continue
            end = time.time()
            uncompress_time += end - start

            extracted_folder_path = next(tmp_directory_path.iterdir())
            files = [str(path) for path in extracted_folder_path.iterdir()]
            print(f"Convertie {files}")
            start = time.time()
            convert.convert(files, str(output_tileset_dir),
                            crs_out=CRS.from_epsg(srs_out),
                            crs_in=CRS.from_epsg(2154),
                            force_crs_in=True,
                            overwrite=True)  # we already tested for tileset.json
            end = time.time()
            conversion_time += end - start

    if at_least_one_success:
        # Merge the new (and old) tiles
        print("Fusionne tous les tilesets")
        merger.merge(args.out_folder, overwrite=True, verbose=0)
        print_stats(download_time, uncompress_time, conversion_time)
    else:
        print("Aucun téléchargement n'a abouti, arrêt du programme.")
