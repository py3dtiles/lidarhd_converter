import argparse
import os
from dotenv import load_dotenv

from lidarhd_converter import lidarhd

LIDARHD = 'lidarhd'

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--no-verify-ssl', action='store_true', help="Ne valide pas les certificats SSL")

    parser.add_argument('geometry_file', type=str, help="Le fichier contenant les géométries permettant de filtrer les parties du lidarhd. Seul les fichiers intersectant cette géométrie seront téléchargés et convertis. Le CRS de la géométrie doit être le même que celui du lidar hd lui-même (EPSG:2154)")
    parser.add_argument('out_folder', type=Path, help="Le dossier où les tilesets seront synchronisés")
    parser.add_argument('-t', '--tmp-directory', type=Path, help="Le dossier où les fichiers temporaires seront écrits. Peut être renseigné avec la variable d'environnement TMP_DIRECTORY")
    parser.add_argument('-m', '--lidar-metadata-url', type=str, help="L'url complète pour télécharger les métadonnées du LidarHD. Peut être renseigné avec la variable d'environnement LIDAR_METADATA_URL")
    parser.add_argument('-d', '--lidar-hd-endpoint', type=str, help="Le endpoint pour télécharger les fichiers LidarHD. Peut être renseigné avec la variable d'environnement LIDAR_HD_ENDPOINT. Il faut indiquer le protocole (http:// ou https://). Par défaut, l'url présent dans le catalogue n'est pas modifié.")
    parser.add_argument('--lidar-endpoint-regex', type=str, help="L'expression régulière utilisée pour remplacer les urls du catalogue. Utile si le catalogue utilisé n'est pas le catalogue public. Par défaut: https://wxs.ign.fr/.*/telechargement/prepackage/")
    parser.add_argument('-s', '--srs-out', type=str, help="Le SRS de destination. Peut être renseigné avec la variable d'environnement SRS_OUT")
    parser.add_argument('-f', '--force', action='store_true', help="Télécharge et convertit les fichiers LidarHD même s'ils existent déjà dans le dossier")
    parser.add_argument('-l', '--layer', type=int, help="L'index de la couche s'il y en a plusieurs dans le fichier 'geometry_file'")
    args = parser.parse_args()

    lidarhd.import_lidar(args)

if __name__ == '__main__':
    main()
