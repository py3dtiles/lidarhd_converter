# Lidar HD converter

Tool to extract and convert lidar from [french Lidar HD](https://geoservices.ign.fr/lidarhd), filtered by a polygon.

NOTE: this is a WIP, a lot of cleaning (and english translation) are still needed.
